<?php

use Illuminate\Database\Seeder;

class SectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


       $section = \App\Models\Section::create([
            'section_name'=>'construction_services',
            'title'=>'نقوم بإنشاء المباني التي تحتاجها',
            'content'=>'يشرح المُنشئ كيف يمكنك الاستمتاع باتجاهات الأرضيات الراقية مثل الخشب المنسوج والأرضيات المصفحة الجديدة. كمقاول عام',
        ]);

        for ($i =1;$i<=5;$i++){
            \App\Models\SectionType::create([
                'section_id'=>$section->id,
                'name'=>'test'.$i,
                'content'=>'test content'.$i,
            ]);
        }



    }
}
