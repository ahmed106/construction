<?php

use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $setting = new \App\Models\Setting();
        $setting->create([
            'website_name' => 'Construction',
            'website_email' => 'Construction@gmail.com',
            'website_phone' => 10023456789,
            'website_address' => 'Sudia-Gada',
            'website_facebook' => 'https://www.facebook.com',
            'website_twitter' => 'https://www.twitter.com',
            'website_linked_in' => 'https://www.linkedin.com',
        ]);
    }
}
