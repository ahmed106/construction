@extends('website.layouts.master')
@push('title')
    الرئيسيه
@endpush

@section('content')
    @include('website.includes.sliders')
    <!-- Banner end-->
    <!--site-main start-->
    <div class="site-main">
        <!--top-section-->
        <section class=" ttm-row padding-zero-section skin-border-top res-991-margin_top60 grey-border-bottom
             clearfix">
            <div class="container-fluid g-0">
                <div class="row ">
                    <div class="col-lg-12">
                        <div class="row g-0">
                            @if($services->count()>0)
                                @foreach($services->take(4) as $service)
                                    <div class="col-lg-3 col-md-6 border-right">
                                        <div class="featured-icon-box icon-align-top-content style1">
                                            <div class="featured-icon-box-inner">
                                                <div class="featured-icon">
                                                    <div
                                                        class="ttm-icon ttm-icon_element-fill ttm-icon_element-style-square ttm-icon_element-color-skincolor ttm-icon_element-size-md">
                                                        <i class="flaticon flaticon-house"></i>
                                                    </div>
                                                </div>
                                                <div class="featured-content">
                                                    <div class="featured-title">
                                                        <h3>{{$service->title}}
                                                        </h3>
                                                    </div>
                                                    <div class="featured-desc">
                                                        <p>
                                                            {!! $service->content !!}
                                                        </p>
                                                        <a class="ttm-btn btn-inline ttm-btn-size-md ttm-icon-btn-right ttm-textcolor-darkgreycolor"
                                                           href="{{url('services/'.$service->id)}}">قراءة المزيد !</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif


                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--top-section-end-->
        <!--service-section-->
        <section class="ttm-row service-one-section bg-layer-equal-height clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--section-title -->
                        <div class="section-title title-style-center_text">
                            <div class="title-header">
                                <h3>خدماتنا</h3>
                                <h2 class="title">نقوم بإنشاء المباني <br> التي تحتاجها</h2>
                            </div>
                        </div>
                        <!--section-title-end -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-sm-6 m-sm-auto col-12">
                                <!-- col-img-img-one -->
                                <div class="ttm-bg ttm-col-bgimage-yes col-bg-img-one p-175 res-991-p-0">
                                    <div class="ttm-col-wrapper-bg-layer ttm-bg-layer"></div>
                                    <div class="layer-content">
                                    </div>
                                </div>
                                <!-- col-img-img-one -->
                                <img class="img-fluid ttm-equal-height-image w-100"
                                     src="{{asset('assets/website')}}/images/bg-image/col-bgimage-1.jpg" width="370" height="352"
                                     alt="bgimage-1">
                            </div>
                            @if($services->count()>0)
                                @foreach($services->last()->get()->take(2) as $service)
                                    <div class="col-lg-4 col-12">
                                        <div class="featured-icon-box icon-align-top-content style2">
                                            <div class="featured-icon-box-inner">
                                                <div class="featured-icon">
                                                    <div
                                                        class="ttm-icon ttm-icon_element-onlytxt ttm-icon_element-size-md ttm-icon_element-color-skincolor">
                                                        <i class="flaticon flaticon-house"></i>
                                                    </div>
                                                </div>
                                                <div class="featured-content">
                                                    <div class="featured-sub-title">
                                                        <h6>اكتشف الميزات</h6>
                                                    </div>
                                                    <div class="featured-title">
                                                        <h3>{{$service->title}}</h3>
                                                    </div>
                                                    <div class="featured-desc">
                                                        <p>{!! $service->content !!}</p>
                                                    </div>
                                                    <a class="ttm-btn btn-inline ttm-btn-size-md ttm-icon-btn-right ttm-btn-color-darkgrey"
                                                       href="{{url('contact-us')}}">احصل على أسعار!
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif


                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--service-section-end-->
        <!--about-section-->
        <section class="ttm-row bg-custom padding-zero-section clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="ttm-bg ttm-left-span ttm-col-bgimage-yes col-bg-img-two">
                            <div class="layer-content">
                                <!--section-title -->
                                <div>
                                    <div class="section-title padding_right45">
                                        <div class="title-header">
                                            <h3>عن الشركة</h3>
                                            <h2 class="title">{{$about?$about->title:''}}</h2>
                                        </div>
                                        <div class="title-desc">
                                            <p>
                                                {!! $about?$about->missions:'' !!}
                                            </p>
                                        </div>
                                    </div>
                                    <!--section-title-end -->
                                    <div
                                        class="ttm-horizontal_sep width-100 margin_top20 margin_bottom10 margin_right45">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <!-- ttm-fid -->
                                            <div class="ttm-fid inside ttm-fid-with-icon ttm-fid-view-lefticon style1">
                                                <div class="ttm-fid-contents">
                                                    <h4 class="ttm-fid-inner">
                                                        <span data-appear-animation="animateDigits" data-from="0"
                                                              data-to="{{$about?$about->prizes_number:''}}" data-interval="5" data-before=""
                                                              data-before-style="sup" data-after="+"
                                                              data-after-style="sub" class="numinate">{{$about?$about->prizes_number:''}}</span>
                                                    </h4>
                                                    <h3 class="ttm-fid-title">الجوائز</h3>
                                                </div>
                                            </div>
                                            <!-- ttm-fid End-->
                                        </div>
                                        <div class="col-md-4">
                                            <!-- ttm-fid -->
                                            <div class="ttm-fid inside ttm-fid-with-icon ttm-fid-view-lefticon style1">
                                                <div class="ttm-fid-contents">
                                                    <h4 class="ttm-fid-inner">
                                                        <span data-appear-animation="animateDigits" data-from="0"
                                                              data-to="{{$about?$about->customers_number:''}}" data-interval="5" data-before=""
                                                              data-before-style="sup" data-after="+"
                                                              data-after-style="sub" class="numinate">{{$about?$about->customers_number:''}}</span>
                                                    </h4>
                                                    <h3 class="ttm-fid-title">عملاؤنا</h3>
                                                </div>
                                            </div>
                                            <!-- ttm-fid End-->
                                        </div>
                                        <div class="col-md-4">
                                            <!-- ttm-fid -->
                                            <div class="ttm-fid inside ttm-fid-with-icon ttm-fid-view-lefticon style1">
                                                <div class="ttm-fid-contents">
                                                    <h4 class="ttm-fid-inner">
                                                        <span data-appear-animation="animateDigits" data-from="0"
                                                              data-to="{{$about?$about->complete_projects_number:''}}" data-interval="5" data-before=""
                                                              data-before-style="sup" data-after="+"
                                                              data-after-style="sub" class="numinate">{{$about?$about->complete_projects_number:''}}</span>
                                                    </h4>
                                                    <h3 class="ttm-fid-title">المشاريع</h3>
                                                </div>
                                            </div>
                                            <!-- ttm-fid End-->
                                        </div>
                                    </div>
                                </div>
                                <div class="ttm_single_image-wrapper imagestyle-one ">
                                    <img class="img-fluid auto_size" src="{{asset('assets/website')}}/images/single-img1.png" alt="single-02">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--about-section-end-->
        <!--process-step-section-->
        <section class="ttm-row process-step-section ttm-bgimage-yes bg-img1 ttm-bg clearfix">
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-lg-12">
                        <!--section-title -->
                        <div class="section-title mb-custom title-style-center_text padding_bottom50 res-375-padding_bottom0">
                            <div class="title-header">
                                <h3>لماذا تختارنا !</h3>
                                <h2 class="title">{{strip_tags($home_service_section->content)}}</h2>
                            </div>
                        </div>
                        <!--section-title-end -->
                    </div>
                </div>
                <!-- row-end -->
                <!-- row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ttm-processbox-style1">
                            <div class="ttm-processbox-content">
                                <div class="ttm-processbox-title">
                                    <h4>{{$home_service_section->title}}</h4>
                                </div>
                                <div class="ttm-circle-img"><img src="{{$home_service_section->image}}" alt="single-01">
                                </div>
                                @foreach($home_service_section->types as $index => $type)
                                    <div class="ttm-boxes ttm-processbox-{{$index}}-box">
                                        <div class="ttm-process-iocn">
                                            <div class="ttm-box-icon">
                                                <img class="flaticon" src="{{$type->image}}" alt="">
                                            </div>
                                        </div>
                                        <div class="ttm-content-box">
                                            <div class="ttm-box-title">
                                                <h4>{{$type->name}}</h4>
                                            </div>
                                            <div class="ttm-process-box-desc">{{strip_tags($type->content)}}</div>
                                        </div>
                                    </div>
                                @endforeach


                            </div>
                        </div>
                    </div>
                </div>
                <!-- row-end -->
            </div>
        </section>
        <!--process-step-section-end-->
        <section class="ttm-row work-section ttm-bgcolor-darkgrey">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <!--section-title-->
                        <div class="section-title title-style-center_text">
                            <div class="title-header">
                                <h3>مشاريعنا</h3>
                                <h2 class="title">خبرة في مجموعة واسعة
                                    من أعمال المشروع</h2>
                            </div>
                        </div>
                        <!--section-title-end-->
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            @if($projects->count()>0)
                                @foreach($projects->take(4) as $project)
                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <!-- featured-imagebox-portfolio -->
                                        <div class="featured-imagebox featured-imagebox-portfolio style3">
                                            <!-- ttm-box-view-overlay -->
                                            <div class="ttm-box-view-overlay ttm-portfolio-box-view-overlay">
                                                <!-- featured-thumbnail -->
                                                <div class="featured-thumbnail">
                                                    <a href="{{url('projects/'.$project->id)}}"> <img class="img-fluid" src="{{asset('images/projects/'.$project->photos->first()->src)}}" alt="image" height="100%" width="100%"></a>
                                                </div><!-- featured-thumbnail end-->
                                                <div class="ttm-media-link">
                                                    <a href="{{url('projects/'.$project->id)}}" class="ttm_link"><i class="fa fa-plus"></i></a>
                                                </div>
                                            </div><!-- ttm-box-view-overlay end-->
                                            <div class="featured-content featured-content-portfolio">
                                                <div class="featured-title">
                                                    <h5><a href="{{url('projects/'.$project->id)}}">{{$project->title}}</a></h5>
                                                </div>

                                            </div>
                                        </div><!-- featured-imagebox-portfolio -->
                                    </div>
                                @endforeach

                            @endif


                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--Blog-section-->
        <section class="ttm-row blog-section clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--section-title -->
                        <div class="section-title title-style-center_text">
                            <div class="title-header">
                                <h3>المقالات</h3>
                                <h2 class="title">أحدث الأخبار والمقالات</h2>
                            </div>
                        </div>
                        <!--section-title-end -->
                    </div>
                </div>
                <div
                    class="row height-custom ttm-bgimage-yes bg-img3 ttm-bg g-0">
                    <div class="ttm-row-wrapper-bg-layer ttm-bg-layer"></div>
                    <div class="col-12">
                        <div class="margin_left45 res-991-margin_left0 margin_right15 res-991-margin_right0">
                            <div class="featured-imagebox-post style7">
                                <div class="featured-content">
                                    @if($blogs->count()>0)
                                        @foreach($blogs->take(2) as $blog)
                                            <div class="featured-icon-box icon-align-before-content icon-ver_align-top padding_bottom20 margin_bottom20 border-bottom">
                                                <div class="featured-icon">
                                                    <img width="86" height="76" class="img-fluid auto_size"
                                                         src="{{$blog->image}}" alt="ttm_single_image-wrapper">
                                                </div>
                                                <div class="featured-content padding_left15 padding_top5">
                                                    <div class="post-meta">
                                                        <a href="{{url('blogs/'.$blog->id)}}">
                                                        <span class="ttm-meta-line byline">
                                                            <i class="far fa-calendar-alt"></i>{{\Illuminate\Support\Carbon::create($blog->created_at)->monthName}} {{\Illuminate\Support\Carbon::create($blog->created_at)->day}}, {{\Illuminate\Support\Carbon::create($blog->created_at)->year}}</span></a>
                                                    </div>
                                                    <div class="featured-title">
                                                        <h4><a href="{{url('blogs/'.$blog->id)}}">
                                                                {{$blog->title}}

                                                            </a></h4>
                                                    </div>
                                                </div>
                                            </div>

                                        @endforeach
                                    @endif


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Blog-section-end-->
        <!--client-section-->
        <section class="ttm-row client-section ttm-bgcolor-grey clearfix">
            <div class="container">
                <div class="row">
                    <h3 class="title">عملائنا الكرام </h3>
                </div>
                <div class="row slick_slider"
                     data-slick='{"slidesToShow": 6, "slidesToScroll": 1, "arrows":false, "autoplay":false, "infinite":true, "responsive": [{"breakpoint":1200,"settings":{"slidesToShow": 5}}, {"breakpoint":1024,"settings":{"slidesToShow": 4}}, {"breakpoint":777,"settings":{"slidesToShow": 3}},{"breakpoint":575,"settings":{"slidesToShow": 2}},{"breakpoint":400,"settings":{"slidesToShow": 1}}]}'>
                    @if($customers->count()>0)
                        @foreach($customers as $customer)
                            <div class="col-lg-12">


                                <div class="client-box style1">
                                    <div class="client-thumbnail">
                                        <img class="img-fluid auto_size" width="160" height="98"
                                             src="{{$customer->image}}" alt="image">
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif


                </div><!-- row end -->
            </div>
        </section>
        <!--client-section-end-->
    </div>
@endsection
