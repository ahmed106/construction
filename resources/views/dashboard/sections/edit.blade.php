@extends('dashboard.layouts.master')
@section('content')
<div class="main-content side-content">
    <div class="container-fluid">
        <div class="inner-body">

            <form action="{{route('sections.update',$section->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')


                <div class="card">
                    <div class="card-header">
                        <h3>تعديل سكشن</h3>
                    </div>
                    <div class="card-body pb-0">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-custom">
                                    <input readonly value="{{$section->section_name}}" type="hidden" name="section_name"
                                        class="form-control">
                                    <span class="input-span d-none">إسم السشكن</span>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="input-custom">
                                    <input value="{{$section->title}}" type="text" name="title" class="form-control">
                                    <span class="input-span">العنوان</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-custom">
                                    <textarea name="content" rows="3"
                                        class="form-control editor">{{$section->content}}</textarea>
                                    <span class="input-span">المحتوي</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-custom">
                                    <label class="">الصورة ( مقاس الصورة -- 670 طول * 770 عرض )</label>
                                    <input id="photo" type="file" name="photo">
                                    <img width="100" id="preview" height="100" src="{{$section->image}}" alt="">
                                </div>
                            </div>
                        </div>

                        <hr>
                        {{--                            <button id="add_type_btn" class="btn btn-warning"><i class="fa fa-plus"></i> إضافه نوع جديد--}}
                        {{--                            </button>--}}

                        <h3>لماذا تختارنا</h3>
                        <br>

                        <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>العنوان</th>
                                    <th>المحتوي</th>
                                    <th>الصورة</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($section->types as $index => $type)
                                @php
                                $index +=1;
                                @endphp
                                <tr>
                                    <td>
                                        <div class="input-custom">
                                            <input type="hidden" name="type[{{$index}}][id]" value="{{$type->id}}">
                                            <input type="text" value="{{$type->name}}" class="form-control"
                                                name="type[{{$index}}][name]">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-custom">
                                            <input type="text" value="{{$type->content}}" class="form-control"
                                                name="type[{{$index}}][content]">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-custom">
                                            <input type="file" id="photo" class="photo" name="type[{{$index}}][photo]">
                                        </div>
                                    </td>
                                    <td>
                                            <img width="100" id="preview" height="100" src="{{$type->image}}" alt="">
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>

                        </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button class="btn ripple btn-primary" type="submit"><i class="fe fe-save"></i> حفظ</button>
                    </div>
                </div>
            </form>

        </div>
    </div> <!-- End Main Content-->
</div> <!-- End Page -->
@endsection
@push('js')
<script>
    let count = '{{count($section->types)}}';
    count = ++count;



    // $('#add_type_btn').on('click', function (e) {
    //     e.preventDefault();
    //     if (count > 5) {
    //         Swal.fire({
    //             icon: 'error',
    //             title: 'عذراً ...',
    //             text: 'عدد الأنواع لابد أن يكون مساوياً  ل5  أو أقل !',
    //
    //         });
    //     } else {
    //         let content = `
    //     <div class="card-content">
    //         <i class="fa fa-times delete_card"></i>
    //         <div class="row">
    //             <div class="col-md-6">
    //                 <div class="input-custom">
    //                     <input type="text" class="form-control" name="type[${count}][name]">
    //                     <span class="input-span">الاسم</span>
    //                 </div>
    //             </div>
    //             <div class="col-md-6">
    //             <div class="row">
    //                 <div class="col-md-6">
    //                     <div class="input-custom">
    //                         <input type="file" class="form-control" name="type[${count}][photo]">
    //                          <span class="input-span">الصوره</span>
    //                      </div>
    //                 </div>
    //                 <div class="col-md-6"></div>
    //             </div>
    //
    //             </div>
    //             <div class="col-md-12">
    //                 <div class="input-custom">
    //                     <input type="text" class="form-control" name="type[${count}][content]">
    //                     <span class="input-span">المحتوي</span>
    //                 </div>
    //             </div>
    //         </div>
    //     </div>
    //
    // `;
    //         $('#add_new_type').append(content);
    //         count++
    //
    //
    //         console.log(count)
    //     }
    // });


    // $(document).on('click', '.delete_card', function () {
    //     Swal.fire({
    //         title: 'هل أنت متأكد',
    //         text: "",
    //         icon: 'warning',
    //         showCancelButton: true,
    //         confirmButtonColor: '#3085d6',
    //         cancelButtonColor: '#d33',
    //         confirmButtonText: 'نعم',
    //         cancelButtonText: 'لا'
    //     }).then((result) => {
    //         if (result.isConfirmed) {
    //             $(this).parent().remove();
    //
    //             count--
    //             count = count < 1 ? 1 : count;
    //
    //         }
    //     });
    //
    // });

</script>
@endpush
