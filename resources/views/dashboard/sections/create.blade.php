@extends('dashboard.layouts.master')
@section('content')
<div class="main-content side-content">
    <div class="container-fluid">
        <div class="inner-body">

            <form action="{{route('sections.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-header">
                        <h3>إضافه سكشن</h3>
                    </div>
                    <div class="card-body pb-0">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-custom">
                                    <input type="text" name="section_name" class="form-control">
                                    <span class="input-span">إسم السشكن</span>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="input-custom">
                                    <input value="{{old('title')}}" type="text" name="title" class="form-control">
                                    <span class="input-span">العنوان</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-custom">
                                    <textarea name="content" rows="3"
                                        class="form-control editor">{{old('content')}}</textarea>
                                    <span class="input-span">المحتوي</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-custom">
                                    <label class="">الصورة ( مقاس الصورة -- 670 طول * 770 عرض )</label>
                                    <input id="photo" type="file" name="photo">
                                    <img width="100" id="preview" height="100" src="{{asset('default.svg')}}" alt="">
                                </div>
                            </div>
                        </div>
                        <hr>
                        <button id="add_type_btn" class="btn btn-warning"><i class="fa fa-plus"></i> إضافه نوع جديد
                        </button>
                        <br>
                        <div class="card-new" id="add_new_type"></div>
                    </div>
                    <div class="card-footer">
                        <button class="btn ripple btn-primary" type="submit"><i class="fe fe-save"></i> حفظ</button>
                    </div>
                </div>
            </form>

        </div>
    </div> <!-- End Main Content-->
</div> <!-- End Page -->
@endsection
@push('js')
<script>
    let count =1;
    $('#add_type_btn').on('click', function (e) {
        e.preventDefault();

        if(count > 5){
            Swal.fire({
                icon: 'error',
                title: 'عذراً ...',
                text: 'عدد الأنواع لابد أن يكون مساوياً  ل5  أو أقل !',

            })
        }else{
            let content = `
            <div class="card-content">
                <i class="fa fa-times delete_card"></i>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-custom">
                            <input type="text" class="form-control" name="type[${count}][name]">
                            <span class="input-span">الاسم</span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-custom">
                            <input type="file" class="form-control" name="type[${count}][photo]">
                            <span class="input-span">الصوره</span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="input-custom">
                            <input type="text" class="form-control" name="type[${count}][content]">
                            <span class="input-span">المحتوي</span>
                        </div>
                    </div>
                </div>
            </div>

        `;
            $('#add_new_type').append(content);
            count++
        }
    });


    $(document).on('click','.delete_card',function (){
        Swal.fire({
            title: 'هل أنت متأكد',
            text: "",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'نعم',
            cancelButtonText: 'لا'
        }).then((result) => {
            if (result.isConfirmed) {
                $(this).parent().remove();

                count--
                count = count < 1? 1 : count;

            }
        });

    });

</script>
@endpush
