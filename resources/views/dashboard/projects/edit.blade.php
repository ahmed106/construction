@extends('dashboard.layouts.master')
@section('content')
    <div class="main-content side-content">
        <div class="container-fluid">
            <div class="inner-body">

                <form action="{{route('projects.update',$project->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="card">
                        <div class="card-header">
                            <h3>إضافه مشروع</h3>
                        </div>
                        <div class="card-body pb-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input value="{{$project->title}}" type="text" name="title" class="form-control">
                                        <span class="input-span">العنوان</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input value="{{$project->customer}}" type="text" name="customer" class="form-control">
                                        <span class="input-span">العميل</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <textarea name="content" rows="3" class="form-control editor">{{$project->content}}</textarea>
                                        <span class="input-span">المحتوي</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">

                                        <input type="date" value="{{$project->end_date}}" name="end_date" class="form-control">
                                        <span class="input-span">تاريخ الانتهاء</span>
                                    </div>
                                </div>
                            </div>
                            <div class="input-custom">
                                <span class="input-span">الصور ( مقاس الصورة -- 800 طول * 1200 عرض )</span>
                                <div class="input-images"></div>
                            </div>

                            <div class="accordion-container">
                                <div class="set">
                                    <a href="#">
                                        بيانات السيو SEO
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    <div class="box-custom">
                                        <div class="flex-divs">
                                            <div class="input-custom">
                                                <input value="{{$project->meta_title}}" type="text" name="meta_title" class="form-control">
                                                <span class="input-span">عنوان الميتا</span>
                                            </div>
                                            <div class="input-custom">
                                                <input value="{{$project->meta_keywords}}" type="text" name="meta_keywords" class="form-control">
                                                <span class="input-span">الكلمات الدلالية</span>
                                            </div>
                                            <div class="input-custom">
                                                <textarea name="meta_description" class="form-control editor">{!! $project->description !!}</textarea>
                                                <span class="input-span">وصف الميتا</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer">
                            <button class="btn ripple btn-primary" type="submit"><i class="fe fe-save"></i> حفظ</button>
                        </div>
                    </div>
                </form>

            </div>
        </div> <!-- End Main Content-->
    </div> <!-- End Page -->

@endsection

@push('js')
    <script>


        $('.input-images').imageUploader({
            preloaded:  {!! getProjectAlbum($project->id) !!},
            imagesInputName: 'photos',
            preloadedInputName: 'old',
            maxSize: 2 * 1024 * 1024,
            maxFiles: 10,
            delete: function () {
                alert('asd')
            }

        });
    </script>
@endpush
