@extends('dashboard.layouts.master')
@section('content')
    <div class="main-content side-content">
        <div class="container-fluid">
            <div class="inner-body">

                <form action="{{route('about.update',$about->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="card">
                        <div class="card-header">
                            <h3>من نحن</h3>
                        </div>
                        <div class="card-body pb-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input name="title" value="{{$about->title}}" type="text" class="form-control">
                                        <span class="input-span">العنوان</span>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-custom">
                                        <textarea name="content" rows="3" class="form-control editor">{{$about->content}}</textarea>
                                        <span class="input-span">التفاصيل</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-custom">
                                        <textarea name="missions" rows="3" class="form-control editor">{{$about->missions}}</textarea>
                                        <span class="input-span">مهمتنا</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-custom">
                                        <textarea name="vision" rows="3" class="form-control editor">{{$about->vision}}</textarea>
                                        <span class="input-span">رؤيتنا</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-custom">
                                        <textarea name="plans" rows="3" class="form-control editor">{{$about->plans}}</textarea>
                                        <span class="input-span">طموحاتنا</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-custom">
                                        <input name="complete_projects_number" value="{{$about->complete_projects_number}}" type="text" class="form-control">
                                        <span class="input-span">عدد المشروعات المكتملة</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-custom">
                                        <input type="text" name="prizes_number" value="{{$about->prizes_number}}" class="form-control">
                                        <span class="input-span">عدد الجوائز</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-custom">
                                        <input name="customers_number" value="{{$about->customers_number}}" type="text" class="form-control">
                                        <span class="input-span">عدد العملاء</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-custom">
                                        <input name="employee_number" value="{{$about->employee_number}}" type="text" class="form-control">
                                        <span class="input-span">عدد الموظفين</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <label class="">الصورة </label>
                                        <input id="photo" type="file" name="photo">
                                        <img width="100" id="preview" height="100" src="{{$about->image}}" alt="">
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn ripple btn-primary" type="submit"><i class="fe fe-save"></i> حفظ</button>
                        </div>
                    </div>
                </form>

            </div>
        </div> <!-- End Main Content-->
    </div> <!-- End Page -->
@endsection
