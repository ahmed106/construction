@extends('dashboard.layouts.master')
@section('content')
    <div class="main-content side-content">
        <div class="container-fluid">
            <div class="inner-body">

                <form action="{{route('about.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <h3>من نحن</h3>
                        </div>
                        <div class="card-body pb-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input name="title" value="{{$about?$about->title:old('title')}}" type="text" class="form-control">
                                        <span class="input-span">العنوان</span>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-custom">
                                        <textarea name="content" rows="3" class="form-control editor">{{$about?$about->content:old('content')}}</textarea>
                                        <span class="input-span">التفاصيل</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-custom">
                                        <textarea name="missions" rows="3" class="form-control editor">{{$about?$about->missions:old('missions')}}</textarea>
                                        <span class="input-span">مهمتنا</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-custom">
                                        <textarea name="vision" rows="3" class="form-control editor">{{$about?$about->vision:old('vision')}}</textarea>
                                        <span class="input-span">رؤيتنا</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-custom">
                                        <textarea name="plans" rows="3" class="form-control editor">{{$about?$about->plans:old('plans')}}</textarea>
                                        <span class="input-span">طموحاتنا</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-custom">
                                        <input name="complete_projects_number" value="{{$about?$about->complete_projects_number:old('complete_projects_number')}}" type="text" class="form-control">
                                        <span class="input-span">عدد المشروعات المكتملة</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-custom">
                                        <input type="text" name="prizes_number" value="{{$about?$about->prizes_number:old('prizes_number')}}" class="form-control">
                                        <span class="input-span">عدد الجوائز</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-custom">
                                        <input name="customers_number" value="{{$about?$about->customers_number:old('customers_number')}}" type="text" class="form-control">
                                        <span class="input-span">عدد العملاء</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-custom">
                                        <input name="employee_number" value="{{$about?$about->employee_number:old('employee_number')}}" type="text" class="form-control">
                                        <span class="input-span">عدد الموظفين</span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <label class="">الصورة ( مقاس الصورة -- 412 طول * 512 عرض )</label>
                                        <input id="photo" type="file" name="photo">
                                        @if($about && $about->photo)
                                            <img width="100" id="preview" height="100" src="{{$about->image}}" alt="">
                                        @else
                                            <img width="100" id="preview" height="100" src="{{asset('default.svg')}}" alt="">
                                        @endif

                                    </div>
                                </div>
                            </div>
                            <div class="accordion-container">
                                <div class="set">
                                    <a href="#">
                                        بيانات السيو SEO
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    <div class="box-custom">
                                        <div class="flex-divs">
                                            <div class="input-custom">
                                                <input value="{{$about?$about->meta_title:old('meta_title')}}" type="text" name="meta_title" class="form-control">
                                                <span class="input-span">عنوان الميتا</span>
                                            </div>
                                            <div class="input-custom">
                                                <input value="{{$about?$about->meta_keywords:old('meta_keywords')}}" type="text" name="meta_keywords" class="form-control">
                                                <span class="input-span">الكلمات الدلالية</span>
                                            </div>
                                            <div class="input-custom">
                                                <textarea name="meta_description" class="form-control editor">{{$about?$about->meta_description:old('meta_description')}}</textarea>
                                                <span class="input-span">وصف الميتا</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer">
                            <button class="btn ripple btn-primary" type="submit"><i class="fe fe-save"></i> حفظ</button>
                        </div>
                    </div>
                </form>

            </div>
        </div> <!-- End Main Content-->
    </div> <!-- End Page -->
@endsection
