@extends('website.layouts.master')
@push('title')
    {{$page_name}} | {{$service->title}}
@endpush
@section('content')
    <div class="ttm-page-title-row ttm-bg ttm-bgimage-yes ttm-bgcolor-dark clearfix">
        <div class="ttm-row-wrapper-bg-layer ttm-bg-layer"></div>
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="ttm-page-title-row-inner">
                        <div class="page-title-heading">
                            <h2 class="title">{{$page_name}}</h2>
                        </div>
                        <div class="breadcrumb-wrapper">
                                <span>
                                    <a title="Homepage" href="{{url('/')}}">
                                        <i class="themifyicon ti-home"></i> &nbsp;
                                        الرئيسية</a>
                                </span>
                            <span>{{$page_name}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- page-title end -->
    <!--site-main start-->
    <div class="site-main">
        <div class="ttm-row sidebar ttm-sidebar-left clearfix">
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-lg-4 widget-area sidebar-left">
                        <aside class="widget widget-nav-menu">
                            <ul>
                                @foreach($services as $ser)
                                    <li class="{{$ser->id == $service->id ? 'active':''}}"><a href="{{url('services/'.$ser->id)}}">{{$ser->title}}</a></li>
                                @endforeach


                            </ul>
                        </aside>
                        <aside class="widget widget-banner">
                            <div class="ttm-col-bgcolor-yes ttm-bgcolor-darkgrey col-bg-img-nine ttm-col-bgimage-yes ttm-bg">
                                <div class="ttm-col-wrapper-bg-layer ttm-bg-layer">
                                    <div class="ttm-col-wrapper-bg-layer-inner"></div>
                                </div>
                                <div class="layer-content">
                                    <div class="widget-banner-inner">
                                        <h3>عنوان الشركة</h3>
                                        <p>{{$setting->website_address}}</p>
                                        <h3>اتصل بنا</h3>
                                        <p class="mb-0">البريد الالكتروني:{{$setting->website_email}}</p>
                                        <p>اتصل بنا: {{$setting->website_phone}}</p>


                                        <a class="ttm-btn ttm-btn-size-md ttm-btn-shape-square ttm-btn-style-fill ttm-btn-color-skincolor  margin_top10" href="{{url('contact-us')}}">تواصل معنا</a>
                                    </div>
                                </div>
                            </div>
                        </aside>
                    </div>
                    <div class="col-lg-8 content-area">
                        <div class="ttm-service-single-content-area">
                            <div class="ttm-service-description">
                                <div class="padding_bottom20">
                                    <div class="ttm_fatured_image-wrapper">
                                        <img class="img-fluid" src="{{$service->image}}" alt="services-1">
                                    </div>
                                </div>
                                <div class="padding_top30 res-575-padding_top0">
                                    <h3>{{$service->title}}</h3>
                                    <p>
                                        {!! $service->content !!}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- row end -->
            </div>
        </div>
    </div>
@endsection
