<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="HTML5 Template"/>
    <meta name="description" content="Template-2"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {!! $setting?$setting->header:'' !!}
    <title>  @stack('title') | {{$setting->website_name}}  </title>
    @include('website.includes.css')


    <style>
        .float {
            position: fixed;
            width: 60px;
            height: 60px;
            bottom: 40px;
            right: 40px;
            background-color: #25d366;
            color: #FFF !important;
            border-radius: 50px;
            text-align: center;
            font-size: 30px;
            z-index: 100;
            display: flex;
            justify-content: center;
        }

        .my-float {
            margin-top: 16px;
        }
    </style>
</head>

<body>
<!--page start-->
<div class="page">
    <!-- preloader start -->
    <div id="preloader">
        <div id="status"></div>
    </div>
    <!-- preloader end -->
    <!--header start-->
@include('website.includes.header')
<!--header end-->
    <!-- Banner -->
@yield('content')

<!--site-main end-->
    <!--footer start-->
@include('website.includes.footer')

<!--footer end-->
    <!--back-to-top start-->
    <a id="totop" href="#top">
        <i class="fa fa-angle-up"></i>
    </a>
    <!--back-to-top end-->
</div><!-- page end -->

<a href="https://wa.me/{{$setting->website_phone}}" class="float" target="_blank">
    <i class="flaticon flaticon-chat my-float"></i>
</a>
@include('website.includes.js')
{!! $setting?$setting->footer:'' !!}
</body>

</html>
