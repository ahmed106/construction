<header id="masthead" class="header ttm-header-style-01">
    <!-- topbar -->
    <div class="top_bar ttm-bgcolor-skincolor clearfix">
        <div class="container-fluid fullwide">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="top_bar_contact_item ">
                                <div class="top_bar_icon">
                                    <i class="flaticon flaticon-placeholder"></i></div>
                                <div class="top_bar_content">
                                    {{isset($setting) ? $setting->website_address:''}}
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="contact-info">
                                <div class="top_bar_contact_item top_bar_social">
                                    <ul class="social-icons">
                                        <li>
                                            <a class="ttm-social" href="{{$setting?$setting->website_facebook:''}}" rel="noopener"
                                               aria-label="facebook"><i class="fab fa-facebook-f"></i></a></li>
                                        <li>
                                            <a class="ttm-social" href="{{$setting?$setting->website_twitter:''}}" rel="noopener" aria-label="twitter">
                                                <i class="fab fa-twitter"></i></a></li>
                                        <li>
                                            <a class="ttm-social" href="{{$setting?$setting->website_linked_in:''}}" rel="noopener" aria-label="google"><i
                                                    class="fab fa-linkedin-in"></i></a></li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- topbar end -->
    <!-- site-header-menu -->
    <div id="site-header-menu" class="site-header-menu ttm-bgcolor-darkgrey">
        <div class="site-header-menu-inner ttm-stickable-header">
            <div class="container-fluid fullwide">
                <div class="row g-0">
                    <div class="col-lg-12">
                        <!--site-navigation -->
                        <div class="site-navigation d-flex align-items-center">
                            <!-- site-branding -->
                            <div class="site-branding ">
                                <a class="home-link" href="{{url('/')}}" title="" rel="home">


                                    @if($setting->logo)
                                        <img id="logo-img" height="50" width="159" class="img-fluid auto_size"
                                             src="{{asset('images/settings/'. $setting->logo->src)}}" alt="logo-img">
                                    @endif

                                </a>
                            </div><!-- site-branding end -->
                            <div class="border-box-block">
                                <div class="d-flex align-items-center ">
                                    <div class="btn-show-menu-mobile menubar menubar--squeeze">
                                                <span class="menubar-box">
                                                    <span class="menubar-inner"></span>
                                                </span>
                                    </div>
                                    <!-- menu -->
                                    <nav class="main-menu menu-mobile" id="menu">
                                        <ul class="menu">
                                            @foreach($pages as $page)
                                                @if($page->route=='services')
                                                    @if($services->count()>0)
                                                        <li class="mega-menu-item {{active('services')}}">
                                                            <a href="{{url('services/'.$services->first()->id)}}" class="mega-menu-link">{{$page->name}}</a>
                                                            <ul class="mega-submenu">
                                                                @if($services->count() >0)
                                                                    @foreach($services->take(5) as $service)
                                                                        <li><a href="{{url('services/'.$service->id)}}">{{$service->title}}</a></li>
                                                                    @endforeach
                                                                @endif


                                                            </ul>
                                                        </li>
                                                    @endif
                                                @else
                                                    <li class="mega-menu-item {{active($page->route)}} {{ $page->route =='home' & request()->route()->getName() =='home'?'active':''}}">
                                                        <a href="{{url($page->url)}}">{{$page->name}}</a>
                                                    </li>
                                                @endif
                                            @endforeach


                                        </ul>
                                    </nav><!-- menu end -->
                                    <!-- menu end -->
                                </div>
                            </div>
                            <div
                                class="ttm-widget_header d-flex flex-row align-items-center justify-content-end">
                                <div class="widget_info d-flex flex-row align-items-center justify-content-end">
                                    <div
                                        class="widget_icon ttm-icon ttm-icon_element-color-skincolor ttm-icon_element-onlytxt mb-0">
                                        <i class="fas fa-phone-alt"></i></div>
                                    <div class="widget_content">
                                        <p class="widget_title">اتصل بنا الآن!</p>
                                        <h5>{{$setting?$setting->website_phone:''}}</h5>
                                    </div>
                                </div>
                                <div class="widget_info d-flex flex-row align-items-center justify-content-end">
                                    <div
                                        class="widget_icon ttm-icon ttm-icon_element-color-skincolor ttm-icon_element-onlytxt mb-0">
                                        <i class="far fa-comments"></i></div>
                                    <div class="widget_content">
                                        <p class="widget_title mb-0">راسلنا على</p>
                                        <h5 class="mb-0">{{$setting?$setting->website_email:''}}</h5>
                                    </div>
                                </div>
                                {{--                                <div class="header_btn ">--}}
                                {{--                                    <a class="ttm-btn ttm-btn-size-md ttm-btn-shape-square ttm-btn-style-border ttm-btn-color-white"--}}
                                {{--                                       href="{{url('contact-us')}}">احصل على أسعار!</a></div>--}}
                            </div>
                        </div><!-- site-navigation end-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- site-header-menu end-->
</header>
