<footer class="footer widget-footer clearfix">
    <div class="second-footer ttm-bgimage-yes bg-footer ttm-bg ttm-bgcolor-darkgrey">
        <div class="ttm-row-wrapper-bg-layer ttm-bg-layer"></div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 widget-area">
                    <div class="widget widget-latest-tweets clearfix">
                        <div class="widgte-title padding_left10">
                            <h2>هل لديك اي سؤال؟ يرجى الاتصال بفريقنا</h2>
                        </div>
                        <div class="featured-icon-box icon-align-before-content">
                            <div class="featured-icon">
                                <div
                                    class="ttm-icon ttm-icon_element-size-xs ttm-icon_element-color-skincolor ">
                                    <i class="fas fa-phone-alt align-top"></i>
                                </div>
                            </div>
                            <div class="featured-content padding_left0">
                                <div class="featured-title">
                                    <h3 class="mb-0">الهاتف</h3>
                                </div>
                                <div class="featured-desc">
                                    <p>{{$setting->website_phone}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="margin_top20">
                            <div class="featured-icon-box icon-align-before-content">
                                <div class="featured-icon">
                                    <div
                                        class="ttm-icon ttm-icon_element-size-xs ttm-icon_element-color-skincolor ">
                                        <i class="ti ti-email align-top"></i>
                                    </div>
                                </div>
                                <div class="featured-content padding_left0">
                                    <div class="featured-title">
                                        <h3 class="mb-0">البريد الالكتروني</h3>
                                    </div>
                                    <div class="featured-desc">
                                        <p>{{$setting->website_email}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="widget_social padding_top10 clearfix">
                        <div class="social-icons">
                            <ul class="social-icons list-inline">
                                <li><a class="tooltip-top" href="{{$setting->website_facebook}}" rel="noopener" aria-label="facebook"
                                       data-tooltip="Facebook">
                                        <i class="fab fa-facebook-f"></i></a></li>
                                <li><a class="tooltip-top" href="{{$setting->website_twitter}}" rel="noopener" aria-label="instagram"
                                       data-tooltip="twitter">
                                        <i class="fab fa-twitter"></i></a></li>
                                <li><a class="tooltip-top" href="{{$setting->website_linked_in}}" rel="noopener" aria-label="linkedin"
                                       data-tooltip="linkedin">
                                        <i class="fab fa-linkedin-in"></i></a></li>

                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 widget-area">
                    <div class="widget widget_img_gellary clearfix">
                        <h3 class="widget-title">بعض اعمالنا</h3>
                        <div class="img-gallery">
                            <ul>
                                @foreach($projects as $project)
                                    <li>
                                        <a class="ttm_prettyphoto " title="general-builder" data-rel="prettyPhoto"
                                           rel="prettyPhoto[coregallery]" href="{{asset('images/projects/'.$project->photos->first()->src)}}">
                                            <img class="img-fluid" src="{{asset('images/projects/'.$project->photos->first()->src)}}"
                                                 alt="" height="100" width="100"></a>
                                    </li>
                                @endforeach


                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 widget-area">
                    <div class="widget widget-contact clearfix">
                        <h3 class="widget-title">احصل على تقديرات مجانية</h3>
                        <div class="padding_top30">
                            <div class="textwidget widget-text">
                                <div class="call_detail">
                                    <h3 class="fs-20 margin_bottom10 ttm-textcolor-white"><strong
                                            class="ttm-textcolor-skincolor">تواصل معنا :</strong> {{$setting->website_phone}}
                                    </h3>
                                    <div class="padding_top10 padding_bottom10">
                                        <p>معرفة كيفية العمل والتواصل مع العملاء وأصحاب المصلحة الرئيسيين</p>
                                    </div>
                                    <a class="ttm-btn ttm-btn-size-md ttm-btn-shape-square ttm-btn-style-border ttm-btn-color-white w-100 text-center"
                                       href="{{url('contact-us')}}">طلب نموذج عبر الإنترنت</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom-footer-text ttm-bg copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center">
                        <span class="cpy-text"> © 2021 <a href="https://codlop.sa/ar">Codlop</a> جميع الحقوق محفوظة. </span>
                        <ul class="footer-nav-menu">
                            <li><a href="{{url('about')}}">{{\App\Models\Page::where('url','/about')->first()->name}}</a></li>
                            @if($services->count()>0)
                                <li><a href="{{url('services/'.$services->first()->id)}}">{{\App\Models\Page::where('url','/services')->first()->name}}</a></li>
                            @endif

                            <li><a href="{{url('contact-us')}}">{{\App\Models\Page::where('url','/contact-us')->first()->name}}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
