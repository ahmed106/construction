<div class="ttm-slider-wrapper">
    <div class="ttm-slider-wide">
        <rs-module-wrap id="rev_slider_1_1_wrapper" data-source="gallery"
            style="visibility:hidden;background:transparent;padding:0;margin:0px auto;margin-top:0;margin-bottom:0;">
            <rs-module id="rev_slider_1_1" data-version="6.5.7">
                <rs-slides>

                    @if($sliders->count() > 0)
                    @foreach($sliders as $index => $slider)
                    <rs-slide data-links="{{$slider->link}}" data-key="rs-{{$index}}" data-title="Slide" data-in="o:0;"
                        data-out="a:false;">
                        <img src="{{asset('assets/website')}}/revolution/assets/dummy.png" alt="slide-img"
                            title="clouds-img.png" width="2450" height="1378" class="rev-slidebg tp-rs-img rs-lazyload"
                            data-lazyload="{{asset('assets/website')}}/revolution/assets/clouds-img.png" data-no-retina>

                        <div class="desc-slider">
                            <p>صيانة القصيم</p>
                            <h3>الجودة والنتائج</h3>
                            <div class="content-slide">
                                <i class="flaticon-house"></i>
                                <div class="left-cont">
                                    <h4>{{$slider->title}}</h4>
                                    <div>
                                    {{$slider->content}}
                                    </div>
                                </div>
                            </div>
                        </div>


                        <rs-layer id="{{$index}}" data-type="image" data-rsp_ch="on"
                            data-xy="xo:-700px,-700px,-439px,-270px;" data-text="w:normal;s:20,20,12,7;l:0,0,15,9;"
                            data-dim="w:2450px,2450px,1537px,948px;h:1378px,1378px,863px,532px;" data-frame_1="sp:1500;"
                            data-frame_999="o:0;st:w;" data-loop_0="x:300px;"
                            data-loop_999="x:100;sp:9999;st:500;yym:t;" style="z-index:8;"><img
                                src="{{asset('assets/website')}}/revolution/assets/dummy.png" alt="slide-img"
                                class="tp-rs-img rs-lazyload" width="2450" height="1378"
                                data-lazyload="{{asset('assets/website')}}/revolution/assets/clouds-img-2.png"
                                data-no-retina>
                        </rs-layer>

                        <rs-layer id="{{$index}}" data-type="image" data-rsp_ch="on"
                            data-xy="xo:-350px,-350px,-219px,-135px;y:m;yo:40px,40px,25px,15px;"
                            data-text="w:normal;s:20,20,12,7;l:0,0,15,9;"
                            data-dim="w:1920px,1920px,1204px,742px;h:1101px,1101px,690px,425px;" data-vbility="t,t,f,f"
                            data-frame_1="sp:1000;" data-frame_999="o:0;st:w;" style="z-index:9;"><img
                                src="{{asset('assets/website')}}/revolution/assets/dummy.png" alt="slide-img"
                                class="tp-rs-img rs-lazyload" width="1920" height="1101"
                                data-lazyload="{{asset('images/sliders/'.$slider->photo->src)}}" data-no-retina>
                        </rs-layer>



                    </rs-slide>
                    @endforeach
                    @endif


                </rs-slides>
            </rs-module>
        </rs-module-wrap>
        <!-- END REVOLUTION SLIDER -->
    </div>
</div>
@push('js')
<script>
    $('rs-slide').on('click', function (e) {
        let link = $(this).data('links');

        window.open(link);

    })

</script>

@endpush
