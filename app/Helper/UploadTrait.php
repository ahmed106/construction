<?php

namespace App\Helper;

use Illuminate\Support\Facades\File;

trait UploadTrait
{
    public function upload($file, $path)
    {
        $file_name = rand(100, 100000) . '_' . $file->getClientOriginalName();
        if (!file_exists(public_path('images/' . $path))) {
            mkdir(public_path('images/' . $path), 0777, true);
        }
        $file->move(public_path('images/' . $path), $file_name);
        return $file_name;
    }//end of upload function

    public function uploadAlbum($files, $path)
    {

        $photos = [];
        foreach ($files as $file) {
            $photos[] = $this->upload($file, $path);
        }
        return $photos;

    }//end of uploadAlbum function

    public function deleteOldPhoto($path, $photo)
    {
        File::delete(public_path($path) . '/' . $photo);

    }//end of deleteOldPhoto function

    public function deleteOldAlbum($path, $files, $old)
    {


        foreach ($files as $file) {


            if (!in_array($file->id, $old)) {

                $this->deleteOldPhoto($path, $file->src);
                $file->delete();

            }
        }

    }//end of deleteOldAlbum function


    public function deleteAllOLdPhotos($path, $files)
    {
        foreach ($files as $file) {
            $this->deleteOldPhoto('images/projects/', $file->src);
            $file->delete();
        }

    }//end of  deleteAllOLdPhotos function


}
