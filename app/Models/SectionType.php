<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SectionType extends Model
{
    protected $table ='section_types';
    protected $guarded = [];

    protected $appends = ['image'];

    public function section(){
        return $this->belongsTo(Section::class,'section_id');

    }//end of section function


    public function photo(){

        return $this->morphOne(Photo::class,'photoable');

    }//end of photo function

    public function getImageAttribute()
    {
        if ($this->photo == '') {
            return asset('default.svg');

        }
        return asset('images/section_types/' . $this->photo->src);
    }//end of  function
}
