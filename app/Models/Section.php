<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $table = 'sections';

    protected $with = ['types'];

    protected $guarded = [];

    protected $appends = ['image'];

    public function photo()
    {
        return $this->morphOne(Photo::class, 'photoable');

    }//end of photo function

    public function getImageAttribute()
    {
        if ($this->photo == '') {
            return asset('default.svg');

        }
            return asset('images/sections/' . $this->photo->src);
    }//end of  function

    public function types(){
            return $this->hasMany(SectionType::class,'section_id','id');

    }//end of types function
}
