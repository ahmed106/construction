<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Project;

class ProjectController extends Controller
{
    public function index()
    {
        $page_name = Page::where('url','/projects')->first()->name;
        $projects = Project::get();
        return view('website.pages.projects.index', compact('projects','page_name'));

    }//end of index function

    public function show($id)
    {
        $page_name = Page::where('url','/projects')->first()->name;
        $project = Project::findOrFail($id);
        $projects = Project::select(['id', 'title'])->where('id', '!=', $id)->get()->take(3);


        return view('website.pages.projects.show', compact('project', 'projects','page_name'));
    }//end of show function
}
