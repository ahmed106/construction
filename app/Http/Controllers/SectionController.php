<?php

namespace App\Http\Controllers;

use App\Helper\UploadTrait;

use App\Http\Requests\SectionRequest;
use App\Models\Photo;
use App\Models\Section;
use App\Models\SectionType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use ZipStream\File;

class SectionController extends Controller
{
    use UploadTrait;

    public function index()
    {

        return view('dashboard.sections.index');
    }


    public function create()
    {
        return view('dashboard.sections.create');
    }

    public function data()
    {
        $model = 'sections';
        $sections = Section::with('photo')->get();
        return DataTables::of($sections)
            ->addColumn('check_item', function ($raw) {
                return '<input type="checkbox" name="items[]" class="check_item" value="' . $raw->id . '">';
            })
            ->addColumn('photo', function ($raw) {
                return '<img width="100" height="80"  src="' . $raw->image . '" >';
            })
            ->addcolumn('actions', function ($raw) use ($model) {

                return view('dashboard.includes.actions', compact('raw', 'model'));
            })
            ->rawColumns(['photo' => 'photo', 'check_item' => 'check_item'])
            ->make(true);

    }//end of data function


    public function store(SectionRequest $request)
    {

        $data = $request->validated();

        unset($data['photo'], $data['type']);

        DB::beginTransaction();

        $section = Section::create($data);

        if ($request->hasFile('photo')) {
            $photo = $this->upload($request->photo, 'sections');
            $section->photo()->create([
                'src' => $photo,
                'type' => 'section',
            ]);
        };

        if($request->type){
            for ($x = 1; $x <= count($request->type); $x++) {

                $section_type = $section->types()->create([
                    'name' => $request->type[$x]['name'],
                    'content' => $request->type[$x]['content'],
                ]);

                if (array_key_exists('photo', $request->type[$x])) {
                    $photo = $this->upload($request->type[$x]['photo'], 'section_types');
                    $section_type->photo()->create([
                        'src' => $photo,
                        'type' => 'section_types',
                    ]);
                };
            };
        }



        DB::commit();
        return redirect()->route('sections.index')->with('success', 'تم إضافه البيانات بنجاح');

    }


    public function edit(Section $section)
    {


        return view('dashboard.sections.edit', compact('section'));
    }

    public function update(SectionRequest $request, Section $section)
    {


        $data = $request->validated();
        unset($data['photo']);
        DB::beginTransaction();
        $section->update($data);
        if ($request->hasFile('photo')) {
            $photo = $this->upload($request->photo, 'sections');
            $section->photo ? $this->deleteOldPhoto('images/sections/', $section->photo->src)
                & $section->photo()->update(['src' => $photo]) : $section->photo()->create(['src' => $photo, 'type' => 'section']);
        }


        for ($x = 1; $x <= count($request->type); $x++) {

            if (array_key_exists('id', $request->type[$x])) {

                $section_type = SectionType::findOrFail($request->type[$x]['id']);

                $section_type->update([
                    'name' => $request->type[$x]['name'],
                    'content' => $request->type[$x]['content'],
                ]);

                if (array_key_exists('photo', $request->type[$x])) {
                    if ($section_type->photo != '') {
                        $this->deleteOldPhoto('images/section_types', $section_type->photo->src);
                        $section_type->photo()->delete();
                    }

                    $photo = $this->upload($request->type[$x]['photo'], 'section_types');
                    $section_type->photo()->create([
                        'src' => $photo,
                        'type' => 'section_type'
                    ]);

                }
            } else {
                $section_type = $section->types()->create([
                    'name' => $request->type[$x]['name'],
                    'content' => $request->type[$x]['content'],
                ]);

                if (array_key_exists('photo', $request->type[$x])) {

                    $photo = $this->upload($request->type[$x]['photo'], 'section_types');
                    $section_type->photo()->create([
                        'src' => $photo,
                        'type' => 'section_type'
                    ]);

                }
            }


        }
        DB::commit();

        return redirect()->route('sections.index')->with('success', 'تم تعديل البيانات بنجاح');


    }


    public function destroy(Section $section)
    {
        $section->photo ? $this->deleteOldPhoto('images/sections/', $section->photo->src) & $section->photo()->delete() : '';
        Section::destroy($section->id);
        return redirect()->route('sections.index')->with('success', 'تم حذف البيانات بنجاح');
    }

    public function bulkDelete(Request $request)
    {
        foreach ($request->items as $item) {
            $section = Section::findOrFail($item);
            $this->destroy($section);
        }
        Section::destroy($request->items);
        return redirect()->route('sections.index')->with('success', 'تم حذف البيانات بنجاح');


    }//end of bulkDelete function
}
